#include "csv.h"

#include "QObject"
#include "QString"
#include "QDebug"
#include "QProcess"

class GPIOhandler;
GPIOhandler *gpio_ = NULL;

namespace comm {
namespace motorcontrol {

CSV::CSV(DS18B20 *temp, Vacuum *vac)
{
    vacuum = vac;
    temperature = temp;
    myDelay = 0;

    gpio_ = &Singleton<GPIOhandler>::Instance();

    // Set up gpio
    if (-1 == gpio_->GPIOExport(POUT)
            || -1 == gpio_->GPIOExport(PTANK)
//            || -1 == gpio_->GPIOExport(PRESET)
            || -1 == gpio_->GPIOExport(PSSR))
    {
        qDebug() << "Error in GPIO";
        return;
    }

    /// Set GPIO directions
    if (-1 == gpio_->GPIODirection(POUT, OUT)
            || -1 == gpio_->GPIODirection(PTANK, IN)
//            || -1 == gpio_->GPIODirection(PRESET, OUT)
            || -1 == gpio_->GPIODirection(PSSR, OUT))
    {
        qDebug() << "Error in GPIO";
        return;
    }

    /// enable Motor thru SSR
    gpio_->GPIOWrite(PSSR, MOTORSTART);
}


void CSV::run()
{
//    qDebug() << "Local time is:" << local;

    int i=0;
    while (++i)
    {
//        qint64 now = QDateTime::currentMSecsSinceEpoch();
        qint64 endWhile = QDateTime::currentMSecsSinceEpoch() + myDelay;
        QDateTime UTC(QDateTime::currentDateTimeUtc());
        QDateTime local(UTC.toLocalTime());

        // Tank full?
        tankfull = (gpio_->GPIORead(PTANK) != 0);

        mutex.lock();
        qDebug() << i << " , " << temperature->getCelsius() <<
                    " , " << vacuum->getADS1015_A3() <<
                    " , " << tankfull*1000 << " , "  << local;
        mutex.unlock();

        while (QDateTime::currentMSecsSinceEpoch() < endWhile)
        {
            // Not be a busy wait; should be >= 1 jiffie
            // /usr/include/asm-generic/params.h says HZ = 100
            msleep(50);
        }
    }
}

void CSV::setDelay(qint64 d)
{
    myDelay = d;
}

} //namespace motorcontrol
} //namespace comm
