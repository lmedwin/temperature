/*****************************************************************************/
/*                        SapGuzzler Intelligent Motor Control               */
/*  (C) Copyright 2016 JDB Associates                                        */
/*  All Rights Reserved.  This is an unpublished work fully protected by the */
/*  copyright laws and is considered a trade secret by the copyright owner.  */
/*****************************************************************************/
#ifndef CHANNELS_H
#define CHANNELS_H
#include <QVariant>
#include <QMap>

struct Channels
{
    Channels() :
        ADS1015_A0(0)
        , ADS1015_A1(0)
        , ADS1015_A2(0)
        , ADS1015_A3(0)
        , Sensoray_A0(0)
        , MCP9808_temp(0.0)
        , BMP280_temp(0.0)
        , BMP280_pres(0.0)
        , DS18B20_temp(0.0)
    {}

    /// ADS1015 channel mappings
    double ADS1015_A0;
    double ADS1015_A1;
    double ADS1015_A2;
    double ADS1015_A3;
    double Sensoray_A0;
    float  MCP9808_temp;
    float BMP280_temp;
    float BMP280_pres;
    double DS18B20_temp;
    bool tankfull;
};

#endif // CHANNELS_H
