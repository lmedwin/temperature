/*****************************************************************************/
/*                        SapGuzzler Intelligent Motor Control               */
/*  (C) Copyright 2016 JDB Associates                                        */
/*  All Rights Reserved.  This is an unpublished work fully protected by the */
/*  copyright laws and is considered a trade secret by the copyright owner.  */
/*****************************************************************************/
#ifndef VACUUM_H
#define VACUUM_H

#include <QtCore>
#include <QObject>

#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <iostream>

#include <wiringPi.h>
#include "ads1015.h"
#include "ds18b20.h"

#define  BASE_PIN       0x64
#define  ADS1015_PIN    BASE_PIN+2
#define  MCP9808_PIN    0x18
#define  BMP280_PIN     0x77
#define  MOTORSTART     1
#define  MOTORSTOP      0

#define PTANK   17


namespace comm {
namespace motorcontrol {

class Vacuum : public QThread
{
    Q_OBJECT
public:
    Vacuum();
    ~Vacuum() {}

    void setDelay (qint64 d);   // Delay in milliseconds
    void run();

    QThread* mythread;
    QMutex mutex;

    qint64 myDelay;

    /// ADS1015 channel mappings
    double ADS1015_A0;
    double ADS1015_A1;
    double ADS1015_A2;
    double ADS1015_A3;

    double scaledVal;

    // Have to access temperatures through the get/set interface
    // It uses a mutex here...
    double getADS1015_A3();
    void setADS1015_A3(double value);

private:
    double      measuredMinVal;
    double      measuredMaxVal;
    double      mappedMinVal;
    double      mappedMaxVal;

    bool        firstTime;
};

} //namespace motorcontrol
} //namespace comm

#endif // VACUUM_H
