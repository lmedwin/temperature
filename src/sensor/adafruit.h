/*****************************************************************************/
/*                        SapGuzzler Intelligent Motor Control               */
/*  (C) Copyright 2016 JDB Associates                                        */
/*  All Rights Reserved.  This is an unpublished work fully protected by the */
/*  copyright laws and is considered a trade secret by the copyright owner.  */
/*****************************************************************************/
#ifndef ADAFRUIT_H
#define ADAFRUIT_H

#include <QtCore>
#include <QObject>

#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <iostream>

#include <wiringPi.h>
#include "ads1015.h"
#include "ds18b20.h"
#include "vacuum.h"
#include "channels.h"
//#include "sapguzzler.h"

#define  BASE_PIN       0x64
#define  ADS1015_PIN    BASE_PIN+2
#define  MCP9808_PIN    0x18
#define  BMP280_PIN     0x77
#define  MOTORSTART     1
#define  MOTORSTOP      0

#define PTANK   17


namespace comm {
namespace motorcontrol {

class Adafruit : public QThread
{
    Q_OBJECT
public:
    Adafruit(DS18B20 *temp, Vacuum *vac);
    ~Adafruit() {}

    void setDelay (qint64 d);   // Delay in milliseconds
//    void run();

    void getSignalValue(QString signalName, Channels &signalvals);
    double translate(double value, double lminVal, double lmaxVal,
                               double rminVal, double rmaxVal);

    QThread* mythread;
    QMutex mutex;

    qint64 myDelay;

    /// ADS1015 channel mappings
    double ADS1015_A0;
    double ADS1015_A1;
    double ADS1015_A2;
    double ADS1015_A3;

    double scaledVal;

    double temper;

    DS18B20 *temperature;
    Vacuum *vacuum;

    // Have to access vacuum through the get/set interface
    // It uses a mutex here...
    double getADS1015_A3();
    void setADS1015_A3(double value);

private:
    double      measuredMinVal;
    double      measuredMaxVal;
    double      mappedMinVal;
    double      mappedMaxVal;

    bool        firstTime;
};

} //namespace motorcontrol
} //namespace comm

#endif // ADAFRUIT_H
