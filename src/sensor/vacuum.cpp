/*****************************************************************************/
/*                        SapGuzzler Intelligent Motor Control               */
/*  (C) Copyright 2016 JDB Associates                                        */
/*  All Rights Reserved.  This is an unpublished work fully protected by the */
/*  copyright laws and is considered a trade secret by the copyright owner.  */
/*****************************************************************************/
#include "vacuum.h"
#include <QDateTime>
#include <QDebug>

namespace comm {
namespace motorcontrol {

Vacuum::Vacuum()
{
    /// setup interface for ADC readback
    ads1115Setup(ADS1015_PIN, ADS1015_ADDRESS);

    myDelay = 0;

    ADS1015_A0     = 0;
    ADS1015_A1     = 0;
    ADS1015_A2     = 0;
    ADS1015_A3     = 0;

#ifdef NEVER
// From the xml file:
        <measuredMinVal>7632</measuredMinVal>
        <measuredMaxVal>14832</measuredMaxVal>
        <mappedMinVal>18.0</mappedMinVal>
        <mappedMaxVal>0.0</mappedMaxVal>
#endif // NEVER

    measuredMinVal = 7632; //config->getMeasuredMinVal();
    measuredMaxVal = 14832; // config->getMeasuredMaxVal();
    mappedMinVal = 18.0; // config->getMappedMinVal();
    mappedMaxVal = 0.0; // config->getMappedMaxVal();

}

void Vacuum::run()
{
    int i=0;
    while (++i)
    {
        qint64 endWhile = QDateTime::currentMSecsSinceEpoch() + myDelay;
        QDateTime UTC(QDateTime::currentDateTimeUtc());
        QDateTime local(UTC.toLocalTime());

        // Get pressure value
        setADS1015_A3(analogRead(ADS1015_PIN+3));
//        qDebug() << "Pressure thread " << i << "pressure " <<
//                    getADS1015_A3()  << local;
        while (QDateTime::currentMSecsSinceEpoch() < endWhile) {
            msleep(20);   // Should not be a busy wait; should be > 1 jiffie
        }
    }
}

void Vacuum::setADS1015_A3(double value)
{
    mutex.lock();
    ADS1015_A3 = value;
    mutex.unlock();
}

double Vacuum::getADS1015_A3()
{
double retval;

    mutex.lock();
    retval = ADS1015_A3;
    mutex.unlock();

    return retval;
}

void Vacuum::setDelay(qint64 d)
{
    myDelay = d;
}

} //namespace motorcontrol
} //namespace comm

