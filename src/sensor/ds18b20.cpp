#include "ds18b20.h"

#include <QtCore>
#include "QObject"
#include "QString"
#include "QDebug"
#include "QProcess"
#include "QRegExp"

namespace comm {
namespace motorcontrol {

DS18B20::DS18B20()
{
    setCelsius(0);
    myDelay = 0;
}


void DS18B20::findDir()
{
    QProcess proc;
    QProcess proc2;
    QStringList file;
    QString milliCelsiusString;

    // Precompute the command to cat the correct temperature file
    QString command("ls /sys/bus/w1/devices/");
    proc.start(command);

    proc.waitForReadyRead();
    results = proc.readAllStandardOutput();
    proc.waitForFinished(1000);

    QRegExp rx("(28\\S+).*\n");
    rx.exactMatch(QString(results));

    file << rx.cap(1);
    if (file[0].isNull())
    {
        qDebug() << "No temperature probe is attached";
//        setCelsius(0);
        return;
    }
    command2 = "cat /sys/bus/w1/devices/" + file[0] + "/w1_slave";

    proc2.start(command2);
    proc2.waitForReadyRead();
    results = proc2.readAllStandardOutput();
    proc2.waitForFinished(1000);

//    qDebug("ds18b20 returned %s", results.toStdString().c_str());
    QRegExp rx2(".*t=(-?\\d*)\n$");
    rx2.exactMatch(QString(results));
    milliCelsiusString = rx2.cap(1);
    double mCelsius = QString(milliCelsiusString).toDouble();

    setCelsius(mCelsius);
    setFahrenheit(((getCelsius()/1000.0) * (9.0/5.0)) + 32);
}

double DS18B20::getTemperature()
{
    if (tempscale == "F")
        tempval = getFahrenheit();
    else if (tempscale == "C")
        tempval = getCelsius() / 1000.0;
    return tempval;
}

void DS18B20::run()
{
    int i=0;
    while (++i)
    {
        qint64 endWhile = QDateTime::currentMSecsSinceEpoch() + myDelay;
        QDateTime UTC(QDateTime::currentDateTimeUtc());
        QDateTime local(UTC.toLocalTime());
        findDir();
//        qDebug() << "Temperature thread " << i << "temp " <<
//                  getCelsius() / 1000.0 << local;
        while (QDateTime::currentMSecsSinceEpoch() < endWhile) {
            msleep(20);   // Should not be a busy wait; should be > 1 jiffie
        }
    }
}

double DS18B20::getFahrenheit() {
    double retval;

    mutex.lock();
    retval = fahrenheit;
    mutex.unlock();
    return retval;
}

void DS18B20::setFahrenheit(double v) {
    mutex.lock();
    fahrenheit = v;
    mutex.unlock();
}

double DS18B20::getCelsius() {
    double retval;

    mutex.lock();
    retval = celsius;
    mutex.unlock();
    return retval;
}

void DS18B20::setCelsius(double v) {
    mutex.lock();
    celsius = v;
    mutex.unlock();
}

void DS18B20::setDelay(qint64 d)
{
    myDelay = d;
}

} //namespace motorcontrol
} //namespace comm
