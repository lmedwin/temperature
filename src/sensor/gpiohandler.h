#ifndef GPIOHANDLER_H
#define GPIOHANDLER_H
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <iostream>

//#include "sapguzzler.h"

#include <wiringPi.h>

/// GPIO pins
///
#define IN  0
#define OUT 1

#define LOW  0
#define HIGH 1

#define PIN     24      // Control NimbeLink Power
#define POUT    6       // Not used

#define PTANK   17      // Is tank full?
#define PSSR    25      // Control pump
#define PRESET  27      // Reset NimbeLink

class GPIOhandler
{
public:
    GPIOhandler();
    ~GPIOhandler();

    /// GPIO access via the sysfs interface
    int GPIORead(int pin);
    int GPIOWrite(int pin, int value);

    int GPIOExport(int pin);
    int GPIOUnexport(int pin);
    int GPIODirection(int pin, int dir);

};

#endif // GPIOHANDLER_H
