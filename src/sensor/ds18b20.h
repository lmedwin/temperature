#ifndef DS18B20_H
#define DS18B20_H

#include <QtCore>
#include <QObject>

namespace comm {
namespace motorcontrol {

class DS18B20 : public QThread
{
    Q_OBJECT
public:
    DS18B20();
    ~DS18B20() {}

    void setDelay (qint64 d);   // Delay in milliseconds
    void run();

    QThread* mythread;
    QMutex mutex;

    qint64 myDelay;

    void findDir();

    // Have to access temperatures through the get/set interface
    // It uses a mutex here...
    double getFahrenheit();
    void setFahrenheit(double v);
    double getCelsius();
    void setCelsius(double v);

    // Protected by a mutex
    double tempval;

    double getTemperature();
    void setTempScale(QString scale) { tempscale = scale; }

    double fahrenheit;
    double celsius;

private:

    QByteArray results;
    QString command2;

    QString tempscale;
};

} //namespace motorcontrol
} //namespace comm

#endif // DS18B20_H
