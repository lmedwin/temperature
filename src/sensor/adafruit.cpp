/*****************************************************************************/
/*                        SapGuzzler Intelligent Motor Control               */
/*  (C) Copyright 2016 JDB Associates                                        */
/*  All Rights Reserved.  This is an unpublished work fully protected by the */
/*  copyright laws and is considered a trade secret by the copyright owner.  */
/*****************************************************************************/
#include "adafruit.h"
#include "gpiohandler.h"
#include "motorcontrol/singleton.h"

#include <QDebug>

class GPIOhandler;
extern GPIOhandler *gpio_;

namespace comm {
namespace motorcontrol {

//class SapGuzzler;
//extern SapGuzzler *sapguzzler_;

Adafruit::Adafruit(DS18B20 *temp, Vacuum *vac)
{
    vacuum = vac;
    temperature = temp;
    myDelay = 0;

//    ConfigParams *config = NULL;
//    QVariant comp_ = sapguzzler_->components().value(typeid(ConfigParams).name());
//    if (comp_.isValid() && comp_.canConvert<ConfigParams *>())
//    {
//        config = comp_.value<ConfigParams *>();
//    }

    /// Voltage varies from 5 volts, at 30 psi, to 0 at -15 psi.
    /// Display is in inches of Hg; vacuum is positive.
    /// compute mapped(scaled) pressure value
//    double m = (mappedMaxVal-mappedMinVal) / (measuredMaxVal-measuredMinVal);
//    double offset = 37.6;
//    scaledVal = m * signalvals.ADS1015_A3 + offset;

    gpio_ = &Singleton<GPIOhandler>::Instance();

    // Set up gpio exports
    if (-1 == gpio_->GPIOExport(PTANK)
//            || -1 == gpio_->GPIOExport(PRESET)
            || -1 == gpio_->GPIOExport(PSSR))
    {
        qDebug() << "Error in GPIO";
        return;
    }

    /// Set GPIO directions
    if (-1 == gpio_->GPIODirection(PTANK, IN)
//            || -1 == gpio_->GPIODirection(PRESET, OUT)
            || -1 == gpio_->GPIODirection(PSSR, OUT))
    {
        qDebug() << "Error in GPIO";
        return;
    }

    /// enable Motor thru SSR
    gpio_->GPIOWrite(PSSR, MOTORSTART);
}


void Adafruit::getSignalValue(QString signalName, Channels &signalvals)
{
    /// Initialize signal values

//    ConfigParams *config = NULL;
//    QVariant comp_ = sapguzzler_->components().value(typeid(ConfigParams).name());
//    if (comp_.isValid() && comp_.canConvert<ConfigParams *>())
//    {
//        config = comp_.value<ConfigParams *>();
//    }

    // Write GPIO value
    gpio_->GPIOWrite(POUT, 1);

    ADS1015_A3 = analogRead(ADS1015_PIN+3);

    // Why do we need this??
    // adjusted pressure/vacuum measurement
    // signalvals.ADS1015_A3 = scaledVal;

    temper = temperature->getFahrenheit();

//    if (NULL != temperature)
//    {
//        QString tempscale_ = (!config->getTempScale().compare("F", Qt::CaseInsensitive)) ? "F" : "C";
//        myDS18B20->setTempScale(tempscale_);
//        myDS18B20-> handleResults();
//        signalvals.DS18B20_temp = myDS18B20->getTemperature();
//    }

#ifdef SAPCHECK_LOG_VALUES
    // log values for testing
    vacValues["temperature"] = QVariant::fromValue(&signalvals.DS18B20_temp);
    vacValues["vacuum"]      = QVariant::fromValue(&signalvals.ADS1015_A3);
    vacValues["vac_scaled"]  = QVariant::fromValue(&scaledVal);
    vacValues["ads1015_A3"]  = QVariant::fromValue(&signalvals.ADS1015_A3);

    QString fname_ = "ADS3";
    csvdiagnostics_.writeValuesToCsvFile(fname_, vacValues);
#endif // SAPCHECK_LOG_VALUES

    // Is tank full?
    signalvals.tankfull = (gpio_->GPIORead(PTANK) != 0);

    // Write GPIO value
    gpio_->GPIOWrite(POUT, 0);

    return;
}

///
/// \brief Adafruit::translate - translate one range of measurements
///             to another
/// \param value    - value to be mapped
/// \param lminVal  - first range min
/// \param lmaxVal  - first range max
/// \param rminVal  - second range to be mapped min
/// \param rmaxVal  - second range to be mapped max
/// \return - adjusted value be mapped to second range
///
double Adafruit::translate(double value, double lminVal, double lmaxVal,
                           double rminVal, double rmaxVal)
{
    /// figure out how wide the each range is
    double leftspan_ = lmaxVal - lminVal;
    double rightspan_ = rmaxVal - rminVal;

    /// convert the left range into a 0-1 range
    double valueScaled = (value - lminVal) / leftspan_;

    /// convert the 0-1 range into a value in the right range
    return (rminVal + (valueScaled * rightspan_));
}

} //namespace motorcontrol
} //namespace comm
