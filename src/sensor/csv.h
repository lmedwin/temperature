#ifndef CSV_H
#define CSV_H

#include <QtCore>
#include <QObject>
#include "ds18b20.h"
#include "adafruit.h"
#include "vacuum.h"
#include "motorcontrol/singleton.h"
#include "gpiohandler.h"

namespace comm {
namespace motorcontrol {

class CSV : public QThread
{
    Q_OBJECT

public:
    CSV(DS18B20* temp, Vacuum* vac);
    ~CSV() {}

    void setDelay (qint64 d);   // Delay in milliseconds
    void run();

    QThread* mythread;
    QMutex mutex;

    Vacuum* vacuum;
    DS18B20* temperature;
    int tankfull;
    qint64 myDelay;

private:

};

} //namespace motorcontrol
} //namespace comm

#endif // CSV_H
