/*****************************************************************************/
/*                        SapGuzzler Intelligent Motor Control               */
/*  (C) Copyright 2016 JDB Associates                                        */
/*  All Rights Reserved.  This is an unpublished work fully protected by the */
/*  copyright laws and is considered a trade secret by the copyright owner.  */
/*****************************************************************************/
#ifndef SINGLETON_H
#define SINGLETON_H

#include <QMutex>

// Generic Thread-safe singleton pattern
template<class T>
class Singleton
{
public:

    static T& Instance()
    {
        static QMutex mutex;

        mutex.lock();
        static  T _instance;                    // create static instance of this class
        mutex.unlock();

        return _instance;                       // return instance
    }

private:
    explicit Singleton();                       // private (hide) constructor
    ~Singleton();                               // hide destructor
    Singleton(const Singleton &);               // hide copy cnstr
    Singleton& operator=(const Singleton &);    // hide assign op

};

#endif // SINGLETON_H
