#ifndef MOTORACTIONS_H
#define MOTORACTIONS_H

enum Actions {
    APIOpen = 1,
    WriteVoltage,
    ReadOutVoltage
};

#endif // MOTORACTIONS_H
