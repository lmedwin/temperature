/*****************************************************************************/
/*                        SapGuzzler Intelligent Motor Control               */
/*  (C) Copyright 2016 JDB Associates                                        */
/*  All Rights Reserved.  This is an unpublished work fully protected by the */
/*  copyright laws and is considered a trade secret by the copyright owner.  */
/*****************************************************************************/
#include <QApplication>
#include <QDebug>
#include "ds18b20.h"
#include "vacuum.h"
#include "adafruit.h"
#include "csv.h"

namespace comm {
namespace motorcontrol {

/// Temperature controller
DS18B20* ds18b20 = NULL;
CSV* csv = NULL;
Vacuum* vacuum = NULL;

} //namespace motorcontrol
} //namespace comm

int main(int argc, char *argv[])
{

    QCoreApplication::setSetuidAllowed(true);

    QApplication app(argc, argv);
    app.setApplicationName("Temperature");
    app.setOrganizationName("DM Contol Systems");

    // Configure logging into a file
    /*
    QSettings* logSettings=new QSettings(configFileName,QSettings::IniFormat,&app);
    logSettings->beginGroup("logging");
    FileLogger* logger=new FileLogger(logSettings,10000,&app);
    logger->installMsgHandler();
    */

    qWarning("Application has started");

    // Temperature
    comm::motorcontrol::ds18b20 = &Singleton<comm::motorcontrol::DS18B20>::Instance();
    comm::motorcontrol::ds18b20->setDelay(1000);
    comm::motorcontrol::ds18b20->start();

    // Pressure
    comm::motorcontrol::vacuum = &Singleton<comm::motorcontrol::Vacuum>::Instance();
    comm::motorcontrol::vacuum->setDelay(50);
    comm::motorcontrol::vacuum->start();

    // CSV
    comm::motorcontrol::csv = new comm::motorcontrol::CSV(comm::motorcontrol::ds18b20,
                                                          comm::motorcontrol::vacuum);
    comm::motorcontrol::csv->setDelay(50);
    comm::motorcontrol::csv->start();

    return app.exec();
}

//Q_DECLARE_METATYPE(ConfigParams *)

