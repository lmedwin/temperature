TEMPLATE = app
TARGET = temperature

CONFIG += qt
QT += core \
    widgets
CONFIG += debug \
    thread

HEADERS += \
    src/sensor/adafruit.h \
    src/sensor/ads1015.h \
    src/sensor/csv.h \
    src/sensor/gpiohandler.h \
    src/sensor/ds18b20.h \
    src/motorcontrol/singleton.h \
    src/sensor/vacuum.h \
    src/sensor/channels.h
SOURCES += \
    src/main.cpp \
    src/sensor/adafruit.cpp \
    src/sensor/csv.cpp \
    src/sensor/gpiohandler.cpp \
    src/sensor/ds18b20.cpp \
    src/sensor/vacuum.cpp
LIBS += -lwiringPi \
        -lwiringPiDev
FORMS +=
RESOURCES +=


INCLUDEPATH += $$PWD/
INCLUDEPATH += src/
INCLUDEPATH += src/sensor/

DEPENDPATH += $$PWD/
DEPENDPATH += src/
DEPENDPATH += src/sensor

OTHER_FILES +=

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../../../usr/local/lib/release/ -lrpi2
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../../../usr/local/lib/debug/ -lrpi2
else:unix: LIBS += -L$$PWD/../../../../../usr/local/lib/ -lrpi2

INCLUDEPATH += $$PWD/../../../../../usr/local/include
DEPENDPATH += $$PWD/../../../../../usr/local/include

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../../../../../usr/local/lib/release/librpi2.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../../../../../usr/local/lib/debug/librpi2.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../../../../../usr/local/lib/release/rpi2.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../../../../../usr/local/lib/debug/rpi2.lib
else:unix: PRE_TARGETDEPS += $$PWD/../../../../../usr/local/lib/librpi2.a
